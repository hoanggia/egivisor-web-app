import {createStore, applyMiddleware} from 'redux';
import {createLogger} from 'redux-logger';
import apiMiddleWare from './core/apiMiddleWare';
import rootReducer from './RootReducer';

const logger = createLogger();

const createStoreWithMiddleware = applyMiddleware(logger, apiMiddleWare)(createStore);
export function configureStore(initialState) {
	return createStoreWithMiddleware(rootReducer, initialState);
}