import {ActionTypes} from '../../core/constant';

const initialState = {};
export default function SuggestionReducer(state = initialState, action) {
	switch (action.type) {
		case ActionTypes.SUGGESTION_SET:
			const {Suggestion} = action;
			return Suggestion;
		default:
			return state;
	}
}