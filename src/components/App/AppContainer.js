import {connect} from 'react-redux';
import App from './App';
import * as actions from './AppAction';

const mapStateToProps = (state) => {
    return {
        state: state
    }
};

export default connect(mapStateToProps, actions)(App);