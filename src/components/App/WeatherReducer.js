import {ActionTypes} from '../../core/constant';

const initialState = {};
export default function WeatherReducer(state = initialState, action) {
	switch (action.type) {
		case ActionTypes.WEATHER_SET:
			const {Weather} = action;
			return Weather;
		default:
			return state;
	}
}