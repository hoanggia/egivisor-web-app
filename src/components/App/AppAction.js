import {ActionTypes} from '../../core/constant';

export function fetchWeather(inputLocation) {
	return {
		type: ActionTypes.FETCH_WEATHER,
        inputLocation
	};
};

export function setWeather(Weather) {
	return {
		type: ActionTypes.WEATHER_SET,
        Weather
	};
};

export function setSuggestion(Suggestion) {
	return {
		type: ActionTypes.SUGGESTION_SET,
        Suggestion
	};
};