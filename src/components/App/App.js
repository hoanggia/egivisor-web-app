import React from 'react';
import UserInputComponent from '../UserInput/UserInputContainer';
import LocationComponent from '../Location/LocationContainer';
import OutputComponent from '../Output/OutputContainer';
import {Grid, Row, ButtonGroup, Button} from 'react-bootstrap';

const App = function(props) {
	return(
		<div>
			<Grid>
				<Row className="show-grid">
					<h2>Egivisor</h2>
				</Row>
				<Row className="show-grid">
					<UserInputComponent />
				</Row>
				<br />
				<Row className="show-grid">
					<LocationComponent />
				</Row>
				<br />
				<Row className="show-grid">
					<ButtonGroup vertical block>
						<Button bsStyle="success" onClick={() => props.fetchWeather(props.state.inputLocation)}>Submit</Button>
					</ButtonGroup>
				</Row>
				<hr />
				<Row className="show-grid">
					<OutputComponent />
				</Row>
			</Grid>
		</div>
	)
}
export default App