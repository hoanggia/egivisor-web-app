import {connect} from 'react-redux';
import LocationComponent from './LocationComponent';
import * as actions from './LocationAction';

const mapStateToProps = (state) => {
	return {
	}
};

export default connect(mapStateToProps, actions)(LocationComponent);