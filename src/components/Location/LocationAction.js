import {ActionTypes} from '../../core/constant';

export function setLocation(inputLocation) {
	return {
		type: ActionTypes.LOCATION_SET,
		inputLocation
	};
};