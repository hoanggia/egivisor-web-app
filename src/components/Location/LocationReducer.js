import {ActionTypes} from '../../core/constant';

const initialState = {};
export default function LocationReducer(state = initialState, action) {
	switch (action.type) {
		case ActionTypes.LOCATION_SET:
			const {inputLocation} = action;
			return inputLocation;	
		default:
			return state;
	}
}