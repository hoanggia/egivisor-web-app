import React, {Component} from 'react';
import {FormControl, Row, Col, DropdownButton, MenuItem, Button} from 'react-bootstrap';

class LocationComponent extends Component {
	constructor() {
		super();
		this.state={
			type: 'City Name'
		};
		this.type=[
			'City Name',
			'Coordinate'
		];
	}

	handleClick(type){
		this.setState({type: type});
	}

	getCoordinate(){
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition((data) =>{
				const Location = {
					latitude: data.coords.latitude,
					longtitude: data.coords.longitude
				}
				this.props.setLocation({typeLocation: this.type[1], Location: Location})
			})
		} else { 
			this.props.setLocation({typeLocation: this.type[1], Location: 'No GPS device'})
		}
	}

	renderType(){
		switch (this.state.type) {
			case (this.type[0]): 
				return(<FormControl type='text' 
									placeholder='Please enter your location city' 
									onChange={(formdata) => this.props.setLocation({typeLocation: this.type[0], Location: formdata.target.value})}
					   />)
			default: 
				return(<Button bsStyle="warning" onClick={this.getCoordinate.bind(this)}>Set Position</Button>)
		}
	}

	render() {
		return (
			<Row className="show-grid">
				<Col xs={2} md={2} lg={2}>
					<DropdownButton title={this.state.type} id="bg-nested-dropdown btn-block">
						{
							this.type.map((type) => {
								return <MenuItem key={type} onClick={() => this.handleClick(type)}>{type}</MenuItem>
							})
						}
					</DropdownButton>
				</Col>
				<Col xs={10} md={10} lg={10}>
					{this.renderType()}
				</Col>
			</Row>
		)
	}
}

export default LocationComponent