/**
 * Created by Vale on 5/22/2017.
 */
import {connect} from 'react-redux';
import UserInputComponent from './UserInputComponent';
import * as actions from './UserInputAction';

const mapStateToProps = (state) => {
  	return {
    	Metric: state.Metric,
  	};
};

export default connect(mapStateToProps, actions)(UserInputComponent);
//export default connect(({UserInputState}) => ({UserInputState}))(UserInput);