import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {ButtonGroup, Button, Row, Col} from 'react-bootstrap';

class ButtonScale extends Component {
  handleSelect(eventKey) {
    this.props.handleSelect(this.props.text, eventKey);
  }

  renderButton(id) {
    var buttons = [1,2,3,4,5].map((e) => {
      return (e === id)
        ? <Button key={e} active>{e}</Button>
        : <Button key={e} onClick={() => this.handleSelect(e)}>{e}</Button>;
    })
    return buttons;
  }

  render() {
    return (
      <div>
        <Row className="show-grid">
					<ButtonGroup>
						{this.renderButton(this.props.active)}
						<Button bsStyle="primary" bsSize={this.props.size}>{this.props.text}</Button>
					</ButtonGroup>
        </Row>
        <br />
      </div>
    );
  }
}
//================================================================================================


export default class UserInputComponent extends Component {
	static propTypes = {
		UserInputState: PropTypes.object
	}

	handleSelect(key, value) {
		let newState = {...this.props}
		newState.Metric[key] = value
		this.props.setUserInput(newState)		
	}

	render() {
		return (
			<div>
				{
					Object.entries(this.props.Metric).map((props, id) => {
						return (
							<Col xs={12} md={6} lg={4} key={id}>
								<ButtonScale text={props[0]} active={props[1]} handleSelect={this.handleSelect.bind(this)}/>
							</Col>
						)
					})
				}
			</div>
		)
	}
}

