/**
 * Created by Vale on 5/22/2017.
 */
import {ActionTypes} from '../../core/constant';

const initialState = {};
	
export default function UserInputReducer(state = initialState, action) {
	switch (action.type) {
		case ActionTypes.USERINPUT_SET:
			const {Metric} = action;
			return {...Metric};		
		default:
			return state;
	}
}