import {ActionTypes} from '../../core/constant';

export function setUserInput(UserInputState) {
	return {
		type: ActionTypes.USERINPUT_SET,
		Metric: UserInputState.Metric
	};
};