import React from 'react';

const OutputComponent = function(props) {
    let output = props.Suggestion
    if (typeof output === 'object'){
        let tape = props.Suggestion['inner-tape'].join(', ')
		let cloth = props.Suggestion['outer-cloth'].join(', ')
		let output = 'Use inner tape of ' + tape + ' and outer cloth of ' + cloth;
        return(
            <div>
                <p>{output} </p>
		    </div>
        ) 
    }
	return(
		<div>
            <p>{output} </p>
		</div>
	)
}
export default OutputComponent