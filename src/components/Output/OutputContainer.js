import {connect} from 'react-redux';
import OutputComponent from './Output';

const mapStateToProps = (state) => {
    return {
        Suggestion: state.Suggestion
    }
};

export default connect(mapStateToProps)(OutputComponent);