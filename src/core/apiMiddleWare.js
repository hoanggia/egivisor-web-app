import {ActionTypes, apiURL} from './constant';
import {setWeather, setSuggestion} from '../components/App/AppAction'
import axios from 'axios';

const apiMiddleWare = store => next => action => {
    if (action.type === ActionTypes.FETCH_WEATHER && action.inputLocation.typeLocation !== -1){
        const url = (action.inputLocation.typeLocation === 'City Name')
            ? apiURL.Weather + '&q=' + action.inputLocation.Location
            : apiURL.Weather + '&lat=' + action.inputLocation.Location.latitude + '&lon=' + action.inputLocation.Location.longtitude;
        
        axios.get(url)
			.then(function (response) {
				const newWeather = response.data.weather;
                store.dispatch(setWeather(newWeather));
                axios({ method: 'get',
						url: 'http://egivisor.ap-southeast-2.elasticbeanstalk.com/suggestion/fetch',
						params: action.Metric
				})
				    .then(function (egiresponse){
                        const newSuggestion = egiresponse.data;
						store.dispatch(setSuggestion(newSuggestion));
					})
					.catch(function (egierror){
						console.log(egierror)
					})
            })
			.catch(function (error) {
                if (error.message === 'Request failed with status code 400'){
                    store.dispatch(setSuggestion('Location not found'));
                }   
            });
    }
    next(action);
};

export default apiMiddleWare;