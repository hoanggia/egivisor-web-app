export const ActionTypes = {
	USERINPUT_SET: 'USERINPUT_SET',
	LOCATION_SET: 'LOCATION_SET',
	FETCH_WEATHER: 'FETCH_WEATHER',
	FETCH_EGI: 'FETCH_EGI',
	WEATHER_SET: 'WEATHER_SET',
	SUGGESTION_SET: 'SUGGESTION_SET'
}

export const apiURL= {
	Weather: 'http://api.openweathermap.org/data/2.5/weather?APPID=f9841fea0ed1ef01d340161541f8d2c4',
	Egi: 'http://egivisor.ap-southeast-2.elasticbeanstalk.com/suggestion/fetch'
}
						