/**
 * Created by Vale on 5/22/2017.
 */
import {combineReducers} from 'redux';
import UserInputReducer from './components/UserInput/UserInputReducer';
import LocationReducer from './components/Location/LocationReducer';
import WeatherReducer from './components/App/WeatherReducer';
import SuggestionReducer from './components/App/SuggestionReducer';

export default combineReducers({
	Metric: UserInputReducer,
	inputLocation: LocationReducer,
	Weather: WeatherReducer,
	Suggestion: SuggestionReducer
});