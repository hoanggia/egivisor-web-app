/* eslint-disable */
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './components/App/AppContainer';
import {Provider} from 'react-redux';
import {configureStore} from './store';

const InitialState = {
	'Metric': {
		'water-clarity': 1,
		'fish-activity': 1
	},
	'inputLocation': {
		'typeLocation': 'City Name',
		'Location': ''
	},
	'Weather': {
		'Weather': '',
		'Location': '',
		'lastLocation': ''	
	},
	'Suggestion': 'Welcome to Egivisor'
};
	
const store = configureStore(InitialState);

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
);
window.store = store